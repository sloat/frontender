from setuptools import setup, find_packages

requires = [
        'Jinja2>=2.7',
        'click>=4.0',
        'uwsgi>=2.0',
        ]

version = '0.1-dev'

setup(
    name='frontender',
    author='Matt Schmidt',
    author_email='matt@digitaleye.com',
    version=version,
    url='http://bitbucket.org/sloat/frontender',
    install_requires=requires,
    include_package_data=True,
    packages=find_packages('frontender'),
    description='Framework for simpler websites',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    entry_points = {
        'console_scripts': [
            'frontender = frontender.script:main'
            ]
        }
)
