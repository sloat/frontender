from __future__ import print_function
import click
import os
import sys

from jinja2 import Environment, FileSystemLoader


@click.command()
@click.argument(u'appname')
def main(appname):
    '''This makes tiny frontend sites.'''
    project_path = os.path.join(os.getcwd(), appname)

    try:
        os.mkdir(project_path)
    except OSError:
        print(u'Unable to create project directory. Shutting down.')
        return 1

    static_path = os.path.join(project_path, u'static')

    try:
        os.mkdir(static_path)
    except OSError:
        print(u'Unable to create project static directory. Shutting down.')
        return 2

    content_path = os.path.join(project_path, u'content')
    try:
        os.mkdir(content_path)
    except OSError:
        print(u'Unable to create project content directory. Shutting down.')
        return 3

    data_path = os.path.join(os.path.dirname(__file__), u'data')
    venv_path = os.getenv('VIRTUAL_ENV')

    env = Environment(loader=FileSystemLoader(data_path))
    devtpl = env.get_template('dev.ini.tpl')
    prodtpl = env.get_template('prod.ini.tpl')
    inittpl = env.get_template('app_init.py.tpl')

    with open(os.path.join(project_path, 'dev.ini'), 'w') as f:
        f.write(devtpl.render(app_name=appname, project_path=project_path,
            virtualenv_path=venv_path))

    with open(os.path.join(project_path, 'prod.ini'), 'w') as f:
        f.write(prodtpl.render(app_name=appname))

    with open(os.path.join(project_path, '__init__.py'), 'w') as f:
        f.write(inittpl.render(content_path=content_path))

if __name__ == '__main__':
    sys.exit(main())
