{# vim: set ft=python: #}
from jinja2 import Environment, FileSystemLoader, TemplateNotFound,\
        TemplateSyntaxError
import os

import logging

logger = logging.getLogger(__name__)

content_path = u'{{ content_path }}'
env = Environment(loader=FileSystemLoader(content_path))

def error404(sr, filename):
    sr(u'404 Not Found', [(u'Content-Type', u'text/plain')])
    message = 'The requested file: "%s" was not found.' % filename
    return message.encode('utf-8')


def app(environ, start_response):
    path = environ[u'PATH_INFO'][1:].strip()

    fname = os.path.basename(path)
    if not fname:
        tplfile = u'index.html'
    elif fname.startswith('_'):
        yield error404(start_response, fname)
        return
    elif fname.endswith(u'.html'):
        tplfile = fname
    else:
        tplfile = u'%s.html' % fname

    if fname == path and fname:
        tplfile = fname
    else:
        tplfile = os.path.join(path, tplfile)

    try:
        template = env.get_template(tplfile)
    except (TemplateNotFound, NotADirectoryError):
        yield error404(start_response, tplfile)
        return

    start_response('200 OK', [('Content-Type', 'text/html')])
    yield template.render(env=environ).encode('utf-8')
