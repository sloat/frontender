{# vim: set ft=htmljinja: #}
[uwsgi]
plugin=python
master = 1
processes = 1
http = :5000

{% if virtualenv_path %}
venv = {{ virtualenv_path }}
{% endif %}
module = {{ app_name }}:app

python-autoreload = 1
catch-exceptions = 1

route = /static/(.*) static:{{ project_path }}/static/$1

mount /={{ project_path }}

