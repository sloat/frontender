{# vim: set ft=htmljinja: #}
[uwsgi]
plugin=python

#set these
virtualenv = 
pythonpath = 
uid = 
gid = 

#these are optional
processes = 4
master = 1
socket = /tmp/{{ app_name }}.sock
module = {{ app_name }}:app
touch-reload = %(pythonpath)/reload-app
logto = %(pythonpath)/logs/uwsgi.log
chmod-socket = 666

#
mount /=%(pythonpath)


